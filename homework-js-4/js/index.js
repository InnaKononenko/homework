`use strict`;
let firstNumber;
let secondNumber;
let operation;
do {
  firstNumber = +prompt(`Write first number`);
} while (isNaN(firstNumber) || firstNumber == ``);
do {
  secondNumber = +prompt(`Write second number`);
} while (isNaN(secondNumber) || secondNumber == ``);
do {
  operation = prompt(`Write operation: +  -  *  /`);
} while (
  !(
    operation === `+` ||
    operation === `-` ||
    operation === `*` ||
    operation === `/`
  )
);
const count = () => {
  if (operation === "+") {
    return firstNumber + secondNumber;
  }
  if (operation === "-") {
    return firstNumber - secondNumber;
  }
  if (operation === "*") {
    return firstNumber * secondNumber;
  }
  if (operation === "/") {
    return firstNumber / secondNumber;
  }
};
console.log(count(firstNumber, secondNumber, operation));
