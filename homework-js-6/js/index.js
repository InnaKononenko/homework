"use strict";
let newUser;
const createNewUser = () => {
  newUser = {
    firstName: prompt("Write your name, please"),
    lastName: prompt("Write your last name, please"),
    birthday: prompt("Write your birthday, please", "dd.mm.yyyy"),
    getLogin() {
      return (this.firstName.charAt(0) + this.lastName).toLowerCase();
    },
    getAge() {
      let reverseBirthday = this.birthday.split(".", 3).reverse().join();
      return Math.floor(
        (new Date() - new Date(reverseBirthday).getTime()) /
          (365.25 * 24 * 60 * 60 * 1000)
      );
    },
    getPassword() {
      return (
        this.firstName.charAt(0).toUpperCase() +
        this.lastName.toLowerCase() +
        this.birthday.slice(6, 10)
      );
    },
  };
  return newUser;
};

console.log(createNewUser());
newUser.getLogin();
console.log(newUser.getAge());
console.log(newUser.getPassword());
