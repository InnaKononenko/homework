"use strict";

const eyeElement = document.querySelectorAll("i");
const inputsElement = document.querySelectorAll("input");
const buttonElemet = document.querySelector(".btn");
const formElement = document.querySelector(".password-form");
const errorElem = document.querySelector(".error-password");

eyeElement.forEach((eye) => {
  eye.addEventListener("click", function (event) {
    eye.classList.toggle("fa-eye-slash");
    eye.previousElementSibling.type == "password"
      ? (eye.previousElementSibling.type = "text")
      : (eye.previousElementSibling.type = "password");
  });
});

formElement.addEventListener("submit", function (event) {
  event.preventDefault();
});

buttonElemet.addEventListener("click", function (event) {
  const passwordInvent = document.querySelector(".password-invent");
  const passwordVerify = document.querySelector(".password-verify");
  if (passwordInvent.value === passwordVerify.value) {
    alert("You are welcome");
    if (errorElem.classList.contains("active")) {
      hideError();
    }
  } else {
    showError();
  }
});
function showError() {
  errorElem.classList.add("active");
}

function hideError() {
  errorElem.classList.remove("active");
}
