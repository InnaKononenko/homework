"use strict";

const imgs = Array.from(document.querySelectorAll(".image-to-show"));
const clearButton = document.querySelector(".clear-button");
const restoreButton = document.querySelector(".restore-button");
let isPaused = false;
function showImg(arr) {
  for (const el of arr) {
    if (!isPaused) {
      if (!el.classList.contains("hidden")) {
        el.classList.add("hidden");
        if (arr.indexOf(el) != arr.length - 1) {
          el.nextElementSibling.classList.remove("hidden");
          break;
        } else {
          arr[0].classList.remove("hidden");
        }
      }
    }
  }
}
const showImgInterval = setInterval(showImg, 3000, imgs);
clearButton.addEventListener("click", function () {
  isPaused = true;
});
restoreButton.addEventListener("click", function () {
  isPaused = false;
});
