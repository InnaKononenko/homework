"use strict";

const tabsName = document.querySelectorAll(".services-item");
const tabsDescription = document.querySelectorAll(".services-item-content");

tabsName.forEach((nameTab) => {
  nameTab.addEventListener("click", function () {
    sortTabsServices(nameTab);
  });
});

function sortTabsServices(name) {
  const dataTab = name.dataset.tab;
  document.querySelector(".services-item.active").classList.remove("active");
  name.classList.add("active");
  document
    .querySelector(".services-item-content.active-desc")
    .classList.remove("active-desc");
  document
    .querySelector(`[data-description="${dataTab}-content"]`)
    .classList.add("active-desc");
}

const tabsNameWork = document.querySelectorAll(".our-work-item");
const tabsDescriptionWork = document.querySelectorAll(".work-photo-list");
const buttonLoad = document.querySelector(".button.load");

tabsNameWork.forEach((nameTabWork) => {
  nameTabWork.addEventListener("click", function () {
    sortTabsWork(nameTabWork);
  });
});

function sortTabsWork(name) {
  const dataTab = name.dataset.worktab;
  document.querySelector(".our-work-item.active").classList.remove("active");
  name.classList.add("active");
  tabsDescriptionWork.forEach((element) => {
    const dataTabDescription = element.dataset.workdescription;

    dataTabDescription === dataTab
      ? (element.style.display = "block")
      : (element.style.display = "none");
    if (dataTab === "All") {
      element.style.display = "block";
    }
  });
}

buttonLoad.addEventListener("click", function () {
  loadMore();
});

function loadMore() {
  const liElem = document.querySelectorAll("li.work-photo-list");
  const parent = document.querySelector(".work-photo");
  liElem.forEach((element) => {
    parent.appendChild(element);
  });
  document
    .querySelector(".work-photo.additionally")
    .classList.remove("additionally");
  buttonLoad.style.display = "none";
  parent.style.paddingBottom = "50px";
}
const buttonLoadGallery = document.querySelector(".button.load-gallery");
buttonLoadGallery.addEventListener("click", function () {
  loadMoreGallery();
});

function loadMoreGallery() {
  document
    .querySelector(".gallery-add.additionally")
    .classList.remove("additionally");
  buttonLoadGallery.style.display = "none";
}
const msnry = new Masonry(".grid", {});

new Swiper(".slider", {
  navigation: {
    nextEl: ".swiper-button-forvard",
    prevEl: ".swiper-button-back",
  },
  slidesPerView: 1,
  slidesPerGroup: 1,
  thumbs: {
    swiper: {
      el: ".slider-mini",
      slidesPerView: 4,
    },
  },
  keyboard: {
    enabled: true,
    onlyInViewport: true,
    pageUpDown: true,
  },
  loop: true,
  effect: "fade",
  fadeEffect: {
    crossFade: true,
  },
});
