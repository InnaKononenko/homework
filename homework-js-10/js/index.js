"use strict";

const tabsName = document.querySelectorAll(".tabs-title");
const tabsDescription = document.querySelectorAll(".tab-description");

tabsName.forEach((name) => {
  name.addEventListener("click", function () {
    const dataTab = name.dataset.tab;
    document.querySelector(".tabs-title.active").classList.remove("active");
    name.classList.add("active");
    document
      .querySelector(".tab-description.active-desc")
      .classList.remove("active-desc");
    document
      .querySelector(`[data-description=${dataTab}-content]`)
      .classList.add("active-desc");
  });
});

// const tabsNameWork = document.querySelectorAll(".our-work-item");
// console.log(tabsNameWork);
console.log(1);
// const tabsDescriptionWork = document.querySelectorAll(".work-photo-list");

// tabsNameWork.forEach((name) => {
//   console.log(1);
//   name.addEventListener("click", function () {
//     const dataTab = name.dataset.tab;
//     document.querySelector(".our-work-item.active").classList.remove("active");
//     name.classList.add("active");
//     document
//       .querySelector(".work-photo-list.active")
//       .classList.remove("active-desc");
//     document
//       .querySelector(`[data-worktab-description=${dataTab}]`)
//       .classList.add("active");
//   });
// });
