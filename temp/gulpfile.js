const gulp = require("gulp");
const sass = require("gulp-sass")(require("sass"));

gulp.task("buildStyle", () => {
  return gulp
    .src("./scss/index.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(gulp.dest("./css"));
});

gulp.task("build", gulp.series(["buildStyle"]));
