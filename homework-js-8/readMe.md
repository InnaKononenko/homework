1. DOM - об'єктна модель документа, набір зв'язаних між собою об'єктів, які створюються при читанні html-коду.
2. InnerHTML - дозволяє зчитувати та змінюватитекст з розміткою html, при цьому вона читатиметься браузером, перетворюючись у дом-дерево;
   innerText - дозволяє зчитувати та змінювати текст, при цьому вставляє всі символи в якості тексту, розмітка не працює.
3. Звернутися до елемента можна кількома способами, дивлячись який це елемент:
   по класу - getElementByClassName;
   по id - getElementById;
   по назві - getElementsByName;
   але найбільш популярними та кращими зараз вважаються querySelector та querySelectorAll, тому що вони гнучкі та дозволяють звертатися до елементів по класу, по id, по назві і по тегам. Головне вказати ідентифікатор селектора.
