"use strict";

const paragraph = Array.from(document.getElementsByTagName("p"));
paragraph.forEach((element) => {
  element.style.backgroundColor = `#ff0000`;
});
const optionsListElement = document.getElementById("optionsList");
console.log(optionsListElement);

const parentElement = optionsListElement.parentElement;
console.log(parentElement);

const clildOptionsListElement = optionsListElement.childNodes;
for (const node of clildOptionsListElement) {
  console.log(node.nodeName + node.nodeType);
}

const testParagraphElement = document.querySelector("#testParagraph");
testParagraphElement.innerText = "This is a paragraph";

const mainHeaderElements = document.querySelector(".main-header");
console.log(mainHeaderElements);
const childMainHeaderElements = mainHeaderElements.children;
for (const child of childMainHeaderElements) {
  child.classList.add("nav-item");
}

const sectionTitle = document.querySelectorAll(".section-title");
for (const el of sectionTitle) {
  el.classList.remove("section-title");
}
