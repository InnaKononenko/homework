"use strict";
let newUser = {};
const createNewUser = () => {
  newUser = {
    firstName: prompt("Write your name, please"),
    lastName: prompt("Write your last name, please"),
    getLogin() {
      return (this.firstName.charAt(0) + this.lastName).toLowerCase();
    },
    setFirstName(value) {
      Object.defineProperty(this, "firstName", { value: `${value}` });
    },
    setLastName(value) {
      Object.defineProperty(this, "lastName", { value: `${value}` });
    },
    disableChangingProperties() {
      Object.defineProperty(newUser, "firstName", {
        writable: false,
      });
      Object.defineProperty(newUser, "lastName", {
        writable: false,
      });
    },
  };
  return newUser;
};
console.log(createNewUser());
newUser.disableChangingProperties();
console.log(newUser.getLogin());
newUser.setFirstName(`Inna`);
newUser.setLastName(`Kononenko`);
