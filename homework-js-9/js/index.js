"use strict";
const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const arr2 = ["hello", ["world", "Kiev"], "Kharkiv", "Odessa", "Lviv"];
function outputArrays(array, parentEl = "body") {
  const parentElement = document.querySelector(parentEl);
  const childElement = document.createElement("ul");
  childElement.className = "arr-list";
  parentElement.append(childElement);
  const arrUl = document.createElement("ul");
  array.forEach((element) => {
    const itemChildElement = document.createElement("li");
    childElement.append(itemChildElement);
    itemChildElement.innerText = element;
    if (Array.isArray(element)) {
      element.forEach((el) => {
        itemChildElement.append(arrUl);
        const liArr = document.createElement("li");
        liArr.className = "liArr-item";
        arrUl.append(liArr);
        liArr.innerText = el;
      });
    }
  });
}
outputArrays(arr2, "div");
let timer;
let seconds = 3;
countdown();
function countdown() {
  if (!seconds == 0) {
    document.querySelector("p").innerHTML = seconds;
  }
  seconds--;
  if (seconds < 0) {
    clearTimeout(timer);
  } else {
    timer = setTimeout(countdown, 1000);
  }
}
let timerId = setTimeout(() => (document.body.innerHTML = ""), 3000);
