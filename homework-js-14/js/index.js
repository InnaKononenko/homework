"use strict";

const changeBtn = document.querySelector(".change-theme");
const logo = document.querySelector(".logo-link");
const buttonCreate = document.querySelector(".button-create");
const optionElement = Array.from(document.querySelectorAll(".option-item"));
const inputs = Array.from(document.querySelectorAll("input"));

const arrElements = [changeBtn, logo, buttonCreate, optionElement, inputs];

function changeTheme(arr) {
  arr.forEach((element) => {
    if (Array.isArray(element)) {
      element.forEach((el) => {
        el.classList.toggle("color-theme");
      });
    } else {
      element.classList.toggle("color-theme");
    }
  });
}

changeBtn.addEventListener("click", () => {
  changeTheme(arrElements);
  if (
    localStorage.getItem("colorTheme") === null ||
    localStorage.getItem("colorTheme") === "white"
  ) {
    localStorage.setItem("colorTheme", "blue");
  } else {
    localStorage.setItem("colorTheme", "white");
  }
});
if (localStorage.getItem("colorTheme") === "blue") {
  changeTheme(arrElements);
}
