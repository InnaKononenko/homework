let m = +prompt(`Write first number`);
let n = +prompt(`Write second number`);
while (isNaN(m) || m == "") {
  m = +prompt(`Write first number again`);
}
while (n <= m || isNaN(n) || n == "") {
  n = +prompt(`Write second number again`);
}
nextNumber: for (let i = m; i <= n; i++) {
  for (let j = 2; j < i; j++) {
    if (i % j == 0) continue nextNumber;
  }
  console.log(i);
}
