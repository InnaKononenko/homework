let userNumber = +prompt(`Write number, please`);
while (userNumber < 5 || isNaN(userNumber) || userNumber === ``) {
  userNumber = +prompt(`Sorry, no numbers`);
}
while (!Number.isInteger(userNumber)) {
  userNumber = +prompt(`Sorry, no integer number`);
}
for (let i = 0; i <= userNumber; i++) {
  if (i % 5 === 0) {
    console.log(i);
  }
}
