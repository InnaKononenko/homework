"use strict";
let arr = ["hello", "world", 23, "23", null];
function filterBy(array, type) {
  let newArr = [];
  array.filter((element) => {
    if (typeof element !== type) {
      newArr.push(element);
    }
  });
  return newArr;
}
console.log(filterBy(arr, "string"));
const doc = document.documentElement;
console.log(doc);
